/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)
      // func.call(context, arg1, arg2, ...) - context: это контекст для this. Это значит
      что функция выполниться в контексте context

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/
  let colors = {
    background: 'purple',
    color: 'white'
  }

function func() {
  let h1 = document.createElement( 'h1' );
  h1.innerText = "I know how binding works in JS";
  document.body.appendChild( h1 );
  document.body.style.background = this.background;
  document.body.style.color = this.color;
}

func.call( colors );


//////////////////////////////

function func1() {
  document.body.style.background = this.background;
  document.body.style.color = this.color;
  let h1 = document.createElement( 'h1' );
  h1.innerText = "I know how binding works in JS";
  document.body.appendChild( h1 );
}


//  без этого блочка чего-то не работало...
var setCol = func1.bind( colors );
setCol();

//////////////////////////////

let header = "I know how binding works in JS";

function func2(  ) {

  let h1 = document.createElement( 'h1' );
  h1.innerText = "I know how binding works in JS";
  document.body.appendChild( h1 );
  document.body.style.background = this.background;
  document.body.style.color = this.color;


}

func2.apply( colors , [ header ]);
















