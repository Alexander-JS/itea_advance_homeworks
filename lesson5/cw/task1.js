/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров } со скоростью { speed }
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/



var Train = {
	name: 'Merry Train',
	speed: 90,
	countPAsangers: 320,
	move: function() {
		console.log('Поезд' + this.name + ' везет' + this.countPAsangers + ' пассажиров со скоростью' + this.speed + ' km/h');
	},
	stoped: function() {
		this.speed = 0;
		console.log('Поезд' + this.name + 'остановился. Скорость: ' + this.speed);
	},
	take_pasangers: function( x ) {
		this.countPAsangers += x;
		console.log('Поезд подобрал ' + x + ' пассажиров, теперь тут ' + this.countPAsangers + " пассажиров");
	}
};

Train.move();
Train.stoped();
Train.take_pasangers( 25 );