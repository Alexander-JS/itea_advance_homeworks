/*
  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

const OurSliderImages = [ 'images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];

let myDIV = document.createElement('div');

const crateMiniSlider = function() {
  for(let i = 0; i < OurSliderImages.length; i++){
  let a = document.createElement('img');
      a.onclick = selectIndex;
      a.src = OurSliderImages[i];
      myDIV.appendChild(a);
  }
  document.body.appendChild(myDIV);
}

crateMiniSlider();
//
var images = myDIV.querySelectorAll('img');
//
// console.log(images)

// / / / / / / / / / / / / / / / / / //
  var btn1 = document.getElementById( 'NextSilde' );
  var btn2 = document.getElementById( 'PrevSilde' );
      btn1.onclick = Move;
      btn2.onclick = Move;
// / / / / / / / / / / / / / / / / / //
const slider = document.getElementById( 'slider' );
// задаём картинку по умолчанию (надо сделать чтобы срабатывало по событию load)
document.addEventListener("DOMContentLoaded", show(OurSliderImages, 0));
///////////////////////////////
var counter = 0;
////////////////////////////////
function Move(){

  if( this.id === 'NextSilde' ){
    if( counter >= OurSliderImages.length-1 ){
      counter = 0;
      // console.log('counter < 0 - '+counter);
    } else {
      counter++;
    }
    //////////////
  } else if( this.id === 'PrevSilde' ){
    // console.log( counter );
    if( counter <= 0 ){
      // counter = OurSliderImages.length-1;  
      console.log('counter > arr.length - 1 - '+counter);
    } else {
      counter--;
    }
  }
  show( OurSliderImages, counter );
}
function show( arr, counter ) {
    // slider.querySelector( 'img' ).className = null;
  let img = document.createElement( 'img' );
  img.src = arr[ counter ];
  slider.innerHTML = null;
  slider.appendChild( img );
  function func() {
  slider.querySelector( 'img' ).className = 'new';
  }
  setTimeout(func, 50);
  setClass( images, 'select', counter );
}
function setClass( arr, classN, counter ) {
  for(let i = 0; i < arr.length; i++){
    if(arr[i].className = classN){
      arr[i].className = null;
    }
  }
  arr[counter].className = classN; 
}
//////////
function selectIndex() {
  for ( let i = 0; i < images.length; i++ ){
    if( this.getAttribute( "src" ) === OurSliderImages[ i ]){
      counter = i;
      setClass( images, 'select', i );
      show( OurSliderImages, i );
    }
  }
}
//////////